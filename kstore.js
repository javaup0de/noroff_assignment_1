/** ----------------------- https://jsdoc.app--------
 * @author Ulrich Prinz <mail@ulrichprinz.com>
 * @version 0.1
 * @description <Komputer Store - This Project is an Assignment for the Noroff Java-Fullstack-Developer course.>
 *  ------------------------------------------------------------------*/

// ====================================================================
// Komputer Store
// This Project is an Assignment for the Noroff Java-Fullstack-Developer course.
//     We were asked to build a "small world" of:
//     Work - Where you can 'earn' money
//     Bank - where you can store that money and get loans.
//     Store - where you can spend the money on Laptops.
// More information at:  https://gitlab.com/javaup0de/noroff_assignment_1
// You can see the 'live' version:  http://up0.de/learn/js_assignment_1/
// I decided to keep the pictures local on my server, to allow faster
// loading, but if you required them to be external for the grading,
// it takes 10 seconds to change the URL below to
// ====================================================================

// ====================================================================
// Global Variables and Helper routines
// ====================================================================
var DEBUG = false; //  Set this to true if you want to see Debug Messages - false to turn them off.
if (!DEBUG) console.log = function () {}; /*  I know overriding basic functions is not good style,
                   but I don't want to write a complete logger function for this small project.. */
// ====================================================================
let shopItems = [];
let myCurrencyFormatter = new Intl.NumberFormat("de-DE", { style: "currency", currency: "EUR" });
// A little helper routine to round currency values to 2 decimal points.

/** ---------------------------------------------------
 * @param {float} num - Takes a float and rounds it to 2 decimal points
 *  ---------------------------------------------------*/
function roundCurrencyToTwo(num) {
  return +(Math.round(num + "e+2") + "e-2");
}

// ====================================================================
//  _      __         __        Note to Graders: I know these 'Titles' are
// | | /| / /__  ____/ /__      a bit of overkill for such a small project,
// | |/ |/ / _ \/ __/  '_/      but I like to use them on larger projects, so that
// |__/|__/\___/_/ /_/\_\       maintainers find the right place quickly later!
// ====================================================================
let workPayAccountBalance = 0.0; // In the real world this should be loaded from a Database, but we simplify here!
// --- ACTIVE HTML ELEMENTS ----------------------
const txtWorkAccountBalance = document.getElementById("txtWorkAccountBalance");
const btnWorkRepayLoan = document.getElementById("btnWorkRepayLoan");
const btnWorkToBank = document.getElementById("btnWorkToBank");
const btnWorkWork = document.getElementById("btnWorkWork");

// just a little 'Setter' routine (not OO but still)  to make setting the value of Account and updating the TextElement easier
/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
const workSetAccountBalance = (amount) => {
  workPayAccountBalance = amount;
  txtWorkAccountBalance.innerText = myCurrencyFormatter.format(workPayAccountBalance);
};
workSetAccountBalance(workPayAccountBalance);

/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
const workWork = (x) => {
  console.log("DEBUG: in function: workWork", x);
  workSetAccountBalance((workPayAccountBalance += 100));
  updateLoanElementsVisibility();
};

/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
const workRepayLoan = (x) => {
  console.log("DEBUG: in function: workRepayLoan", x);
  if (workPayAccountBalance <= bankLoanOutstandingBalance) {
    bankSetLoanBalance(bankLoanOutstandingBalance - workPayAccountBalance);
  } else {
    let leftOver = workPayAccountBalance - bankLoanOutstandingBalance;
    bankSetAccountBalance(bankAccountBalance + leftOver);
    bankSetLoanBalance(0);
  }
  workSetAccountBalance(0);
  updateLoanElementsVisibility();
};

/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
const workToBank = (x) => {
  console.log("DEBUG: in function: workToBank:  ");
  if (bankLoanOutstandingBalance > 0.0) {
    console.log("DEBUG: in function: workToBank - OUTSTANDING LOAN CASE", bankLoanOutstandingBalance);
    let amountGoingToLoan = roundCurrencyToTwo(0.1 * workPayAccountBalance);
    bankSetLoanBalance(bankLoanOutstandingBalance - amountGoingToLoan);
    workPayAccountBalance = workPayAccountBalance - amountGoingToLoan;
  }
  bankSetAccountBalance((bankAccountBalance += workPayAccountBalance));
  workSetAccountBalance(0);
  updateLoanElementsVisibility();
};

// -- Connecting Elements with Listeners connecting them to the local JS-function
btnWorkRepayLoan.addEventListener("click", workRepayLoan);
btnWorkToBank.addEventListener("click", workToBank);
btnWorkWork.addEventListener("click", workWork);

// ====================================================================
//    ___            __
//   / _ )___ ____  / /__
//  / _  / _ `/ _ \/  '_/
// /____/\_,_/_//_/_/\_\
// ====================================================================
let bankAccountBalance = 0.0; // In the real world this should be loaded from a Database, but we simplify here!
let bankLoanOutstandingBalance = 0.0; // In the real world this should be loaded from a Database, but we simplify here!
// --- ACTIVE HTML ELEMENTS ----------------------
const txtBankBalance = document.getElementById("txtBankBalance");
const txtBankOutstandingTitle = document.getElementById("txtBankOutstandingTitle");
const txtBankOutstandingLoan = document.getElementById("txtBankOutstandingLoan");
const btnBankGetLoan = document.getElementById("btnBankGetLoan");

// just a 'Setter' for Account and updating the TextElement
/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
const bankSetAccountBalance = (amount) => {
  bankAccountBalance = parseFloat(amount);
  txtBankBalance.innerText = myCurrencyFormatter.format(bankAccountBalance);
};
bankSetAccountBalance(workPayAccountBalance); // Initial run: Set External Element to whatever might have been loaded from a DB..

/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
// just a 'Setter' (not OO but still) for Loan and updating the TextElement
const bankSetLoanBalance = (amount) => {
  bankLoanOutstandingBalance = amount;
  txtBankOutstandingLoan.innerText = myCurrencyFormatter.format(bankLoanOutstandingBalance);
  updateLoanElementsVisibility();
};

/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
// --- LOAN - ELEMENT - VISIBILITY ----------------------------------
// When a loan is outstanding, then show the related fields - and hide the option to get another one.
const updateLoanElementsVisibility = (onOrOff) => {
  if (bankLoanOutstandingBalance > 0 || bankAccountBalance === 0) btnBankGetLoan.style.visibility = "hidden";
  else btnBankGetLoan.style.visibility = "visible";

  if (bankLoanOutstandingBalance === 0 || workPayAccountBalance === 0) btnWorkRepayLoan.style.visibility = "hidden";
  else btnWorkRepayLoan.style.visibility = "visible";

  let loanTextElementsStatus = "visible";
  if (bankLoanOutstandingBalance === 0) loanTextElementsStatus = "hidden";
  txtBankOutstandingTitle.style.visibility = loanTextElementsStatus;
  txtBankOutstandingLoan.style.visibility = loanTextElementsStatus;
};

//  Initalize the state of the "Get a Loan" Button: If account = Zero, then no button!
btnBankGetLoan.style.visibility = "hidden";

// --- GET  A LOAN ----------------------------------------------------------------
const bankGetLoan = (x) => {
  console.log("DEBUG: in function: bankGetLoan", x);
  let maxLoan = bankAccountBalance * 2;
  let desiredLoan = prompt(
    `How much money do you want to load? (Max = ${myCurrencyFormatter.format(maxLoan)})`,
    myCurrencyFormatter.format(0),
  );
  let desiredLoanFloat = parseFloat(desiredLoan);
  if (isNaN(desiredLoanFloat)) {
    console.log("DEBUG: in function: bankGetLoan : Error with desiredLoanFloat", desiredLoanFloat);
    alert(`We're sorry: \n We could not understand: ${desiredLoan}`);
    return;
  }
  if (desiredLoanFloat > maxLoan) {
    alert(
      `We're sorry: \n You asked for ${myCurrencyFormatter.format(
        desiredLoan,
      )} \n which is more than the Max Loan ( ${myCurrencyFormatter.format(maxLoan)}) possible.`,
    );
    return;
  }
  if (desiredLoanFloat < 0) {
    alert(`We're sorry: \n You asked for a NEGATIVE amount? Are you trying to give US a Loan?? ;-)`);
    return;
  }
  desiredLoanFloat = roundCurrencyToTwo(desiredLoanFloat);
  console.log("DEBUG: in function: bankGetLoan :  desiredLoanFloat", desiredLoanFloat);
  bankSetLoanBalance(desiredLoanFloat);
  bankSetAccountBalance((bankAccountBalance += desiredLoanFloat));
};

// -- Connecting Elements with Listeners connecting them to the local JS-function
btnBankGetLoan.addEventListener("click", bankGetLoan);

// ====================================================================
//    ______
//   / __/ /  ___  ___
//  _\ \/ _ \/ _ \/ _ \     & BUY : Here both together because logic is one
// /___/_//_/\___/ .__/             separate in HTML because visually apart
//              /_/
// ====================================================================
// Just as a visual reference of what the objects in the DB look like:
// const carticleObject = {
//   id: 1,
//   title: "Classic Notebook",
//   description: "A little old, but turns on.",
//   specs: [
//     "Has a screen",
//     "Keyboard works, mostly",
//     "32MB Ram (Not upgradable)",
//     "6GB Hard Disk",
//     "Comes with Floppy Disk Reader (Free) - Requires cable",
//     "Good excersice to carry",
//   ],
//   price: 200,
//   stock: 1,
//   active: true,
//   image: "assets/images/1.png",
// };
// ====================================================================
const shopInventory = [];
let currentArticlePrice = 0.0;

// --- ACTIVE HTML ELEMENTS ----------------------
const selShopCatItems = document.getElementById("selShopCatItems");
const txtShopFeatures = document.getElementById("txtShopFeatures");
const imgBuyItem = document.getElementById("imgBuyItem");
const txtBuyTitle = document.getElementById("txtBuyTitle");
const txtBuyDescription = document.getElementById("txtBuyDescription");
const btnBuyItem = document.getElementById("btnBuyItem");
const txtBuyPrice = document.getElementById("txtBuyPrice");

/** ---------------------------------------------------
 *    SHOP: ITEM CHANGED
 * @param {*} selectedOption
 *  ---------------------------------------------------*/
const shopCatItemsChange = (selectedOption) => {
  let selItemNum = selectedOption.target.selectedIndex;
  currentArticlePrice = shopInventory[selItemNum].price;
  const specsWithLinebreak = shopInventory[selItemNum].specs.map(appendLinebreak);
  function appendLinebreak(value, index, array) {
    return value + "\n";
  }
  txtShopFeatures.innerText = specsWithLinebreak.join(" ").replace(",", " ");
  txtBuyTitle.innerText = shopInventory[selItemNum].title;
  txtBuyDescription.innerText = shopInventory[selItemNum].description;
  txtBuyPrice.innerText = myCurrencyFormatter.format(currentArticlePrice);
  imgBuyItem.src = `assets/images/${selItemNum + 1}.jpg`;
  // imgBuyItem.src = `https://noroff-komputer-store-api.herokuapp.com/${shopInventory[selItemNum].image}`;
  // Note : DB Typo: #5 is a png, but database says jpg!
  //  https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png

  // console.log("DEBUG: in function: shopCatItemsChange", selItemNum);
  // console.log("DEBUG: in function: shopCatItemsChange", shopInventory[selItemNum]);
};

/** ---------------------------------------------------
 *    SHOP: BUY
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
const buyItem = (x) => {
  console.log("DEBUG: in function: buyItem", x);
  // @@
  if (currentArticlePrice > bankAccountBalance) {
    alert(`We're sorry: \n You don't have enough funds in your bank account to buy this item!`);
  } else {
    bankSetAccountBalance(bankAccountBalance - currentArticlePrice);
    alert(`Congratulations! \n You have successfully bought this item!`);
    updateLoanElementsVisibility();
  }
};

/** ---------------------------------------------------
 * Not a function, just a logical grouping: Below the Selector is filled with Data.
 *  ---------------------------------------------------*/
// Load the Data for Shop-Items from the URL Resource:
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((urlResponse) => urlResponse.json())
  .then((urlData) => (shopItems = urlData))
  .then((shopItems) => addItemsToShop(shopItems))
  .then(() => initialUpdateOfSelector()); // see below what this does..

// Iterate through all the data we just retrieved
const addItemsToShop = (shopItems) => {
  shopItems.forEach((x) => addOneItemToShop(x));
  return 0;
};

//Fill the Shop Selector with Elements
const addOneItemToShop = (oneArticle) => {
  console.log("In : addOneItemToShop adding: ", oneArticle);
  shopInventory.push(oneArticle);
  const selectorElement = document.createElement("option");
  selectorElement.value = oneArticle.id;
  selectorElement.appendChild(document.createTextNode(oneArticle.title));
  selShopCatItems.appendChild(selectorElement);
};
// Just a little helper function to initially fill all the other fields that depend
// on the selector with contents after creation. The
const initialUpdateOfSelector = () => {
  selShopCatItems.dispatchEvent(new Event("change"));
};

/** ---------------------------------------------------
 * @param {float} num - Takes @@
 *  ---------------------------------------------------*/
// -- Connecting Elements with Listeners connecting them to the local JS-function
selShopCatItems.addEventListener("change", shopCatItemsChange);
btnBuyItem.addEventListener("click", buyItem);
