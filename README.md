# Komputer Store

This Project is an Assignment for the Noroff Java-Fullstack-Developer course.

**[Documentation with Design-Decisions as a PDF](/doc/Assignment_1.pdf)**

**Try it! -> The "live" version is at : http://ulrichprinz.com/learn/js_assignment_1**

We were asked to build a "small world" of:

- **Work** - Where you can 'earn' money
- **Bank** - where you can store that money and get loans.
- **Store** - where you can spend the money on Laptops.

  Here is a screenshot of 1 proposed solution:
  ![screenshot-of-proposed-solution](/doc/images/proposed_result.png)

  and my final result:
  ![screenshot-of-my-solution](/doc/images/my_result.png)

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Once you cloned/downloaded the project, there are just 3 files in the directory:
-index.html

- a .js
- and a .css file
  and a folder "assets" with a few image-files
  Nothing more to do for the Install!

## Usage

To start, **just doubleclick the index.html** -> it will open in your browser!
Here is a description of the 3 areas you saw in the Overview picture:

### Bank

An area where you will store funds and make bank loans

#### 1.1 Balance

The bank shows a “Bank” balance in your currency.
This is the amount available for you to buy a laptop.

#### 1.2 Outstanding Loan (Only visible after taking a loan)

Shows the outstanding Loan value. This should be reduced as loan paid back.

#### 1.3 Get a loan

The Get a loan button will attempt to get a loan from the bank.
When the Get a loan button is clicked, it must show a “Prompt” popup box that allows you to enter an amount.
**Constraints on Get a loan button:**

1. You cannot get a loan more than double of your bank balance (i.e., If you have 500 you cannot get a loan greater than 1000.)
2. You cannot get more than one bank loan before repaying the last loan
3. You may not have two loans at once. The initial loan should be paid back in full.
   TIP: Remember to check for value types before doing mathematical operations

### 2 WORK

An area to increase your earnings and deposit cash into your bank balance

#### 2.1 Pay

The pay or your current salary amount in your currency.
Should show how much money you have earned by “working”.
This money is NOT part of your bank balance.

#### 2.2 Bank Button

The bank button must transfer the money from your Pay/Salary balance to your Bank balance.
Remember to reset your pay/salary once you transfer.
Constraints on Bank button:

1. If you have an outstanding loan, 10% of your salary MUST first be deducted and transferred to the outstanding Loan amount
2. The balance after the 10% deduction may be transferred to your bank account

#### 2.3 Work button

The work button must increase your Pay balance at a rate of 100 on each click.

#### 2.4 Repay Loan button

Once you have a loan, a new button labeled “Repay Loan” should appear.
Upon clicking this button, the full value of your current Pay amount should go towards the outstanding loan and NOT your bank account.
Any remaining funds after paying the loan may be transferred to your bank account

### 3 SHOP

An area to select and display information about the merchandise
The laptops section has 2 parts: laptop selection area (Figure 3) and info section (Figure 4)

#### 3.1 Laptop selection (Figure 3)

Use a select box to show the available computers.
The feature list of the selected laptop must be displayed here. Changing a laptop should update the user interface with the information for that selected laptop.
Laptop API
The data for the laptops will be provided to you via a RESTful API. The endpoint for the API is: https://noroff-komputer-store-api.herokuapp.com/computers
The endpoint will return an array of computers, each computer will have the following properties -> See on the right

#### 3.2 Info section (Figure 4)

The Info section is where the image, name, and description as well as the price of the laptop must be displayed.

##### 3.2.1 Images

The path to the image of a laptop can be found in the response. Remember to use the base URL WITHOUT the computers path.
e.g., https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png

##### 3.2.2 Buy Now button

The buy now button is the final action of your website. This button will attempt to “Buy” a laptop and
validate whether the bank balance is sufficient to purchase the selected laptop.
If you do not have enough money in the “Bank”, a message must be shown that you cannot afford the laptop.
When you have sufficient “Money” in the account, the amount must be deducted from the bank
and you must receive a message that you are now the owner of the new laptop!
